# You want latexmk to *always* run, because make does not have all the info.
# Also, include non-file targets in .PHONY so they are run regardless of any
# file of the given name existing.
.PHONY: main.pdf all clean

# The first rule in a Makefile is the one executed by default ("make"). It
# should always be the "all" rule, so that "make" and "make all" are identical.
all: main.pdf

# CUSTOM BUILD RULES

# In case you didn't know, '$@' is a variable holding the name of the target,
# and '$<' is a variable holding the (first) dependency of a rule.
# "raw2tex" and "dat2tex" are just placeholders for whatever custom steps
# you might have.


# generate tex files from graphviz DOT files
%.tex: %.gv
	dot2tex -tmath -ftikz --figonly $< -o $@

# in case dot2tex doesn't work we can just generate pdf
# also useful for solo preview
%.pdf: %.gv
	dot -Tpdf $< -o $@


# %.tex: %.raw
# 	./raw2tex $< > $@

# %.tex: %.dat
# 	./dat2tex $< > $@

# MAIN LATEXMK RULE

# -pdf tells latexmk to generate PDF directly (instead of DVI).
# -pdflatex="" tells latexmk to call a specific backend with specific options.
# -use-make tells latexmk to call make for generating missing files.

# -interaction=nonstopmode keeps the pdflatex backend from stopping at a
# missing file reference and interactively asking you for an alternative.
# diagram_eiii802.tex
main.pdf: main.tex diagram_ieee802.pdf
	latexmk -pdf -pdflatex="pdflatex -interaction=nonstopmode" -use-make $<

presentation.pdf: presentation.tex
	latexmk -pdf -pdflatex="pdflatex -interaction=nonstopmode" -use-make $<

clean:
	latexmk -c

