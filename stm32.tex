
\section{Komunikacijski protokoli \\ mikrokrmilnika STM32G0}

Za implementacijo 10BASE-T protokola smo izbrali STM32G0 mikrokrmilnik.
Bolj specifično -- izbrali smo \emph{STM32G071GB}.

To je preprost in cenovno privlačen 32-biten mikrokrmilnik s Cortex-M0+ jedrom ter
hitrostjo urinega takta do 64 MHz. Namenjen je predvsem, nadomeščanju 8-bitnih
mikrokrmilnikov.
Kljub relativni preprostosti pa ponuja mnogo perifernih enot, kot so GPIO,
U(S)ART, SPI, I2C, časovniki in DMA \parencite{stm32g071}.

Za razliko od nekaterih zmoglivejših mikrokrmilnikov pa ne vsebuje periferne
enote za Ethernet. To želimo iz obstoječih komponent sestaviti in ustrezno
programsko podpreti sami.

Opišimo vhodno izhodne periferne enote, in kako bi jih lahko uporabili za
implementacijo protokola 10BASE-T.



\subsection{GPIO}

GPIO oziroma splošno namenski vhod in izhod (ang. \emph{general purpose input
output}) so priključki (ang. \emph{pin}) na mikrokrmilniku, ki jih lahko
uporabimo za branje vhodnih vrednosti ali pa nastavljanje izhodni vrednost. To
je najbolj osnoven način digitalne komunikacije, preko katerega lahko
mikrokrmilnik komunicira z okolico.

V vhodnem stanju priključek prebere svoje stanje napetosti. Razločuje lahko le
med dvema stanjema ``0'' ter ``1''. Stanje ``0'' pomeni vrednost blizu 0 V
stanje ``1'' pa pomeni vrednost blizu napajalne napetosti.
V izhodnem stanju pa lahko nastavimo napetost na priključku. Tako kot pri vhodu
imamo le dve možnosti ``0'' ter ``1''.
Smer (vhod ali izhod) lahko izberemo za vsak priključek posebej.

Mikrokrmilnik \emph{STM32G071GB} ima priključke v skupinah po 16 (GPIOA\_0,
GPIOA\_1, \dots GPIOA\_15, GPIOB\_0, GPIOB\_1, \dots GPIOB\_15, \dots). To
pomeni, da lahko z enim samim ukazom (ang. \emph{instruction}) preberemo 16
vhodnih priključkov ali pa nastavimo vrednost 16-ih izhodnih priključkov. To
lahko storimo z branjem oz. pisanjem registrov \emph{GPIO\_IDR}\ (input data
register) ter \emph{GPIO\_ODR} (output data register).

Poleg tega pa \emph{STM32G071GB} ponuja tudi možnost nastavitve (ang.
\emph{set}) in ponastavitve (ang. \emph{reset}) poljubnih priključkov.
To storimo z pisanjem v register \emph{GPIO\_BSRR} (gpio bit set reset register).
\emph{GPIO\_BSRR} je 32-biten register. Če nastavimo bit v enem (ali večih) izmed
spodnjih 16 bitov, potem se pripadajoči priključek nastavi (spremeni na ``1'').
Če pa nastavimo bit enega izmed zgornjih 16 bitov, pa se pripadajoči priključek
ponastavi (spremeni na ``0'') \parencite{stm32g0x1}.

Na primer -- z zapisom 0x00020040 v \emph{GPIO\_BSRR} spremenimo vrednost
priključka 1 na ``0'' ter vrednost priključka 6 na ``1''. Vsi ostali priključki
pa ostanejo nespremenjeni.

% nova stran, da na naslednji strani ne ostane le ena vrstica %
\newpage

Zaradi relativne preprostosti in paralelnost GPIO-jev so lahko ustrezno hitri.
Vseh 16 izhodnih vrednosti lahko nastavimo vsak takt procesorja.
Če mikrokrmilnik deluje z 64 MHz je to:

\begin{equation}
	\SI{64}{\mega\hertz} \cdot \SI{16}{\bit} = \SI{1024}{Mbps}
\end{equation}

To je 1 Gbps. Mi pa želimo ``le'' 10 Mbps.
Seveda pa bi potrebovali še nek zunanji pretvornik, ki bi 16 paralelnih izhodov
pretvoril v enega.
Druga slabost te metode pa je, da med pisanjem v GPIO register procesor ne
more početi nič drugega, kar pomeni, da bi bila implementacija dvosmerne
komunikacije težja, simultana dvosmerna komunikacija pa verjetno ni možna.


\subsection{U(S)ART}

U(S)ART oziroma univerzalni sinhroni ter asinhroni sprejemnik oddajnik (ang.
\emph{universal synchronous and asynchronous receiver transmitter}) je
obojesmerno serijsko vodilo za povezavo dveh vozlišč.
Kot nam pove ime obstajata dve različici; sinhrona ter asinhrona.

Obravnavali bomo asinhrono različico, saj je ta v praksi veliko bolj pogosta.
Za delovanje potrebuje le dve povezavi in skupno ozemljitev.
Povezavi sta TX (pošiljanje) ter RX (sprejemanje). Podatki se pošiljajo z v
naprej dogovorjeno hitrostjo (ang. \emph{baud rate}).
Pomembna značilnost tega protokola je, da lahko vsako vozlišče podatke pošilja
kadarkoli to želi. Medtem ko pošilja podatke, pa jih lahko tudi sprejema.

Po tem vodilu ne moremo pošiljati poljubnih bitov. Pošiljamo jih v okvirjih
(ang. \emph{frame}). Stanje nedejavnega vodila je ``1''. Okvir se začne z
začetnim bitom (ang. \emph{start bit}) vrednosti ``0'', nato sledi v naprej
dogovorjeno število podatkovnih bitov, ter na koncu zaključni bit (ang.
\emph{stop bit}) vrednosti ``1'', ki vodilo povrne v nedejavno stanje.

\begin{figure}[h]
  \centering
  \newcounter{countup}
  \newcommand*{\countup}{\addtocounter{countup}{1}\thecountup}
  \newcommand*{\crst }{\setcounter {countup}{-1}}
  \begin{tikztimingtable}[
    timing/dslope=0.1,
    timing/coldist=2pt,xscale=3,yscale=1,
    timing/metachar={{K}[2]{#1l !{++(0,+.5\yunit)} N[rectangle,scale=.6]{#2} !{++(0,-.5\yunit)} #1l}},
    timing/metachar={{J}[2]{#1h !{++(0,-.5\yunit)} N[rectangle,scale=.6]{#2} !{++(0,+.5\yunit)} #1h}},
  ]
  \crst TX & H K{START} 8{D{\countup}} J{STOP} H \\
  \end{tikztimingtable}
  \caption{Časovni diagram UART \label{manchester}}
\end{figure}


Razdelitev podatkov v okvirje je nujno potrebna zaradi sinhronizacije. Brez
okvirjev sprejemnik ne bi mogel vedeti kje se okvir začne.

Tudi ko mikrokrmilnik zazna, da se je začel nov okvir, takt okvirja ni
sinhroniziran z notranjo uro mikrokrmilnika. Zato mora mikrokrmilnik uporabiti
neko metodo s katero iz vhodnega signala dobi binarne podatke.

Mikrokrmilnik, ki ga uporabljamo, uporablja metodo hitrejšega vzorčenja. Vhodni
signal vzorči 8x ali pa 16x hitreje kot \emph{baud rate} \parencite{stm32g0x1}.
Iz hitrejše vzorčenega signala lahko nato razbere pravilne vrednosti bitov.

UART periferne enote pa žal ne moramo direktno uporabiti za dekodiranje Ethernet
signala, saj je velikost okvirja pri UART-u ponavadi omejena na 16 bitov
velikost Ethernet paketa pa je lahko do 1500 bajtov (12000 bitov).



\subsection{SPI}

SPI -- serijsko vodilo za periferne enote (ang. \emph{serial peripheral
interface}) je vodilo v konfiguraciji nadrejen/podrejen
(angl. \emph{master/slave}).
Nadrejeno vozlišče lahko komunicira z večimi podrejenimi vozlišči
(vendar le z enim naenkrat).

SPI za svoje delovanje potrebuje vsaj 4 povezave \parencite{motorola-spi03}:

\begin{itemize}
\setlength\itemsep{1ex}
\item CLK -- urin takt (ang. \emph{clock})
\item MOSI -- izhod nadrejenega, vhod podrejenih (ang. \emph{master out, slave
  in})
\item MISO -- vhod nadrejenega, izhod podrejenih (ang. \emph{master in, slave
  out})
\item CS oz. SS -- izbira podrejenega (ang. \emph{chip select, slave select}).
  Po ena žica za vsakega podrejenega.
\end{itemize}


Pošiljanje podatkov je sledeče:
najprej nadrejeni s pomočjo CS linije izbere podrejenega s katerim želi
komunicirati. Nato začne na CLK liniji proizvajati takt ter po MOSI liniji
pošiljati bite. Hkrati pa začne podrejeni pošiljati bite po MISO liniji, tako,
da komunikacija poteka v obeh smereh hkrati.

\begin{figure}[h]
  \centering
  \newcounter{countup}
  \newcommand*{\countup}{\addtocounter{countup}{1}\thecountup}
  \newcommand*{\crst }{\setcounter {countup}{-1}}
  \begin{tikztimingtable} [
    timing/dslope=0.1,
    timing/coldist=2pt,xscale=2,yscale=1,
    timing/d/background/.style ={fill=white},
  ]
  CS &   H 19{L} H \\
  CLK &  Ll 16{c}    LL    16{c}  lL \\
  \crst MISO & Z 8{D{\countup}} {3D{\countup}} 7{D{\countup}} U Z \\
  \crst MOSI & Z 8{D{\countup}} {3D{\countup}} 7{D{\countup}} U Z \\
  \extracode
  \begin{pgfonlayer}{background}
  \begin{scope}[semitransparent ,semithick]
  \vertlines[darkgray]{1.5,2.5,...,8.5}
  \vertlines[darkgray]{11.5,12.5,...,18.5}
  \end{scope}
  \end{pgfonlayer}
  \end{tikztimingtable}
  \caption{Časovni diagram SPI \label{spi_timing}}
\end{figure}

Ko nadrejeni pošlje/sprejme željeno število bitov, preneha pošiljati takt po CLK
povezavi ter sprosti CS povezavo.

Po SPI-ju lahko, za razliko od UART-a, pošiljamo poljubno število bitov brez v
naprej dogovorjene oblike.
Če želimo komunicirati z drugim čipom, moramo najprej preveriti njegovo
dokumentacijo, da izvemo kakšno obliko podatkov pričakuje in koliko podatkov bo
lahko procesiral.

Poleg nedoločene oblike tudi hitrost prenašanja ni vnaprej dogovorjena (omejena
je le maksimalna hitrost -- z hitrostjo strojne opreme).
Po SPI ju lahko torej pošljemo 8 bitov, nato malo počakamo (ne da bi sprostili CS
linijo), ter zatem pošljemo naslednjih 8 bitov.

\vspace{\baselineskip}

Obstaja tudi več različnih verzij SPI protokola, ki se razlikujejo v:
\begin{itemize}
  \setlength\itemsep{1ex}
  \item stanju CLK linije (nizko ali visoko) medtem, ko podatkov ne po pošiljamo,
  \item branju podatkov na prvi ali zadnji fronti urinega signala,
  \item številu MISO/MOSI linij -- vzporedno pošiljanje/sprejemanje podatkov.
\end{itemize}

